syntax="proto3";

package proto.value;

import "type.proto";
import "location.proto";

message Module {
  repeated Function functions = 1;
  repeated proto.type.Type struct = 2;
  // TODO recheck
}

message Function {
  string name = 1;
  repeated Argument parameter = 3;
  repeated BasicBlock basicBlocks = 4;
  proto.type.Type returnType = 5;
  // TODO recheck
}

// https://llvm.org/doxygen/classllvm_1_1Value.html

message Value {
  optional string name = 1;
  proto.type.Type type = 2;
  proto.location.Location location = 3;
  proto.location.InstructionText instructionText = 4;
  oneof subclass {
    Argument argument = 5;
    BasicBlock basicBlock = 6;
    InlineAsm inlineAsm = 7;
    MetadataAsValue metadataAsValue = 8;
    User user = 9;
  }
}

message Argument {
  string name = 1;
  proto.type.Type type = 2;
  bool nonNull = 3;
}

message BasicBlock {
  string name = 1;
  repeated Instruction instructions = 2;
  Instruction terminator = 3;
}

message InlineAsm {
  // TODO no subclasses
}

message MetadataAsValue {
  // TODO no subclasses
}

message User {
  oneof subclass {
    Constant constant = 1;
    DerivedUser derivedUser = 2;
    Instruction instruction = 3;
    Operator operator = 4;
  }
}

message Constant {
  oneof subclass {
    BlockAddress blockAddress = 1;
    ConstantAggregate constantAggregate = 2;
    ConstantData constantData = 3;
    ConstantExpr constantExpr = 4;
    DSOLocalEquivalent dsoLocalEquivalent = 5;
    GlobalValue globalValue = 6;
    NoCFIValue noCFIValue = 7;
  }
}

message BlockAddress {
  // TODO no subclasses
}

message ConstantAggregate {
  // TODO
}

message ConstantArray {
  // TODO no subclasses
}

message ConstantStruct {
  // TODO no subclasses
}

message ConstantVector {
  // TODO no subclasses
}

message ConstantData {
  oneof subclass {
    ConstantAggregateZero constantAggregateZero = 1;
    ConstantDataSequential constantDatSequential = 2;
    ConstantFP constantFP = 3;
    ConstantInt constantInt = 4;
    ConstantPointerNull constantPointerNull = 5;
    ConstantTargetNone constantTargetNone = 6;
    ConstantTokenNone constantTokenNone = 7;
    UndefValue undefValue = 8;
  }
}

message ConstantAggregateZero {
  Value return = 1;
  proto.type.Type type = 2;
}

message ConstantDataSequential {
  proto.type.Type type = 1;
  repeated Constant constant = 2;
}

message ConstantFP {
  double value = 1;
}

message ConstantInt {
  int64 value = 1;
  int64 width = 2;
}

message ConstantPointerNull {
  // TODO
}

message ConstantTargetNone {
  // TODO
}

message ConstantTokenNone {
  // TODO
}

message UndefValue {
  // TODO
}

message ConstantExpr {
  Instruction subInstruction = 1;
}

message DSOLocalEquivalent {
  // TODO
}

message GlobalValue {
  oneof subclass{
    GlobalAlias globalAlias = 1;
    GlobalObject globalObject = 2;
  }
}

message GlobalAlias {
  // TODO without subclasses
}

message GlobalObject {
  oneof subclass{
    Function function = 1;
    GlobalFunc globalFunc = 2;
    GlobalVariable globalVariable = 3;
  }
}

message GlobalFunc {
  // TODO without subclasses
}

message GlobalVariable {
  string name = 1;
  // TODO other things
}

message NoCFIValue {
  // TODO
}

message DerivedUser {
  oneof subclass {
    MemoryAccess memoryAccess = 1;
    string name = 2;
  }
}

message MemoryAccess {
  oneof subclass {
    MemoryPhi memoryPhi = 1;
    MemoryUseOrDef memoryUseOrDef = 2;
  }
}

message MemoryPhi {
  // TODO without subclasses
}

message MemoryUseOrDef {
  oneof subclass {
    MemoryUse memoryUse = 1;
    MemoryDef memoryDef = 2;
  }
}

message MemoryUse {
  // TODO without subclasses
}

message MemoryDef {
  // TODO without subclasses
}

message Instruction {
  message AtomicCmpXchgInst {
    // TODO without subclasses
  }
  message AtomicRMWInst {
    // TODO with subclasses
  }
  message BranchInst {
    message UnconditionalBranchInst {
      string basicBlockName = 1;
    }
    message ConditionalBranchInst {
      Value condition = 1;
      string basicBlockTrueBranch = 2;
      string basicBlockFalseBranch = 3;
    }
    oneof subclass{
      UnconditionalBranchInst unconditionalBranchInst = 1;
      ConditionalBranchInst conditionalBranchInst = 2;
    }
  }
  message CatchReturnInst {
    // TODO without subclasses
  }
  message CatchSwitchInst {
    // TODO
  }
  message CleanupReturnInst {
    // TODO
  }
  message CmpInst {
    enum CmpType {
      FCMP_FALSE = 0; ///< 0 0 0 0    Always false (always folded)
      FCMP_OEQ = 1;   ///< 0 0 0 1    True if ordered and equal
      FCMP_OGT = 2;   ///< 0 0 1 0    True if ordered and greater than
      FCMP_OGE = 3;   ///< 0 0 1 1    True if ordered and greater than or equal
      FCMP_OLT = 4;   ///< 0 1 0 0    True if ordered and less than
      FCMP_OLE = 5;   ///< 0 1 0 1    True if ordered and less than or equal
      FCMP_ONE = 6;   ///< 0 1 1 0    True if ordered and operands are unequal
      FCMP_ORD = 7;   ///< 0 1 1 1    True if ordered (no nans)
      FCMP_UNO = 8;   ///< 1 0 0 0    True if unordered: isnan(X) | isnan(Y)
      FCMP_UEQ = 9;   ///< 1 0 0 1    True if unordered or equal
      FCMP_UGT = 10;  ///< 1 0 1 0    True if unordered or greater than
      FCMP_UGE = 11;  ///< 1 0 1 1    True if unordered, greater than, or equal
      FCMP_ULT = 12;  ///< 1 1 0 0    True if unordered or less than
      FCMP_ULE = 13;  ///< 1 1 0 1    True if unordered, less than, or equal
      FCMP_UNE = 14;  ///< 1 1 1 0    True if unordered or not equal
      FCMP_TRUE = 15; ///< 1 1 1 1    Always true (always folded)

      ICMP_EQ = 32;  ///< equal
      ICMP_NE = 33;  ///< not equal
      ICMP_UGT = 34; ///< unsigned greater than
      ICMP_UGE = 35; ///< unsigned greater or equal
      ICMP_ULT = 36; ///< unsigned less than
      ICMP_ULE = 37; ///< unsigned less or equal
      ICMP_SGT = 38; ///< signed greater than
      ICMP_SGE = 39; ///< signed greater or equal
      ICMP_SLT = 40; ///< signed less than
      ICMP_SLE = 41; ///< signed less or equal
    }

    bool isIntPredicate = 1;
    bool isFloatPredicate = 2;
    Value result = 3;
    Value left = 4;
    CmpType type = 5;
    Value right = 6;
  }

  message ExtractElementInst {
    Value return = 1;
    proto.type.Type type = 2;
    Value target = 3;
    Value index = 4;
  }
  message FenceInst {
    // TODO
  }
  message FuncletPadInst {
    // TODO
  }
  message GetElementPtrInst {
    Value result = 1;
    Value target = 2;
    proto.type.Type targetType = 3;
    repeated Value operand = 4;
    // TODO
  }
  message IndirectBrInst {
    // TODO
  }
  message InsertElementInst {
    Value result = 1;
    proto.type.Type type = 2;
    Value source = 3;
    Value element = 4;
    Value index = 5;
  }
  message InsertValueInst {
    Value value = 1;
    Value target = 2;
    Value result = 3;
    proto.type.Type targetType = 4;
    repeated Value operand = 5;
  }
  message LandingPadInst {
    message LandingPadClause {
      enum Type {
        CLEANUP = 0;
        CATCH = 1;
        FILTER = 2;
      }
      Constant constant = 1;
      Type type = 2;
    }
    repeated LandingPadClause clause = 1;
    proto.type.Type type = 2;
    Value result = 3;
  }
  message PHINode {
    message IncomingValue {
      string name = 1;
      Value value = 2;
    }

    Value result = 1;
    proto.type.Type type = 2;
    repeated IncomingValue incomingValue = 3;
  }
  message ResumeInst {
    proto.type.Type type = 1;
    Value resume = 2;
  }
  message SelectInst {
    Value result = 1;
    proto.type.Type type = 2;
    Value condition = 3;
    Value trueValue = 4;
    Value falseValue = 5;
  }
  message ShuffleVectorInst {
    Value result = 1;
    Value vector1 = 2;
    Value vector2 = 3;
    Constant shuffleVector = 4;
    proto.type.Type type = 5;
  }
  message SwitchInst {
    message SwitchInstBranch {
      ConstantInt condition = 1;
      string label = 2;
    }

    Value switchValue = 1;
    string labelDefault = 2;
    repeated SwitchInstBranch branch = 3;
  }
  message UnreachableInst {
    // TODO
  }
  proto.type.Type type = 27;
  proto.location.Location location = 28;
  proto.location.InstructionText instructionText = 29;

  oneof subclass {
    AtomicCmpXchgInst atomicCmpXchgInst = 1;
    AtomicRMWInst atomicRMWInst = 2;
    BinaryOperator binaryOperator = 3;
    BranchInst branchInst = 4;
    CallBase callBase = 5;
    CatchReturnInst catchReturnInst = 6;
    CatchSwitchInst catchSwitchInst = 7;
    CleanupReturnInst cleanupReturnInst = 8;
    CmpInst cmpInst = 9;
    ExtractElementInst extractElementInst = 10;
    FenceInst fenceInst = 11;
    FuncletPadInst funcletPadInst = 12;
    GetElementPtrInst getElementPtrInst = 13;
    IndirectBrInst indirectBrInst = 14;
    InsertElementInst insertElementInst = 15;
    InsertValueInst insertValueInst = 16;
    LandingPadInst landingPadInst = 17;
    PHINode pHINode = 18;
    ResumeInst resumeInst = 19;
    ReturnInst returnInst = 20;
    SelectInst selectInst = 21;
    ShuffleVectorInst shuffleVectorInst = 22;
    StoreInst storeInst = 23;
    SwitchInst switchInst = 24;
    UnaryInstruction unaryInstruction = 25;
    UnreachableInst unreachableInst = 26;
  }
}

message BinaryOperator {
  Value result = 1;

  Value l = 2;
  Value r = 3;

  enum BinaryOperatorType {
    // integer operators
    ADD = 0;
    F_ADD = 1;
    SUB = 2;
    F_SUB = 3;
    MUL = 4;
    F_MUL = 5;
    U_DIV = 6;
    S_DIV = 7;
    F_DIV = 8;
    U_REM = 9;
    S_REM = 10;
    F_REM = 11;

    // Logical operators (integer operands)
    SHL = 12; // Shift left  (logical)
    L_SHR = 13; // Shift right (logical)
    A_SHR = 14; // Shift right (arithmetic)
    AND = 15;
    OR = 16;
    XOR = 17;
  }

  BinaryOperatorType operatorType = 4;
}

message CallBase {
  oneof subclass{
    CallBrInst callBrInst = 1;
    CallInst callInst = 2;
    CGStatepointInst cgStatePointInst = 3;
    InvokeInst invokeInst = 4;
  }
}

message CallBrInst {
  // TODO without subclasses
}

message CallInst {
  message CallInstClassic {
    oneof subclass{
      Function function = 1;
      Value calledFunction = 2;
    }

    // TODO FunctionType can be here
    repeated Value arguments = 3;
    optional Value result = 4;
  }

  message IntrinsicInst {
    message MemIntrinsicBase {
      // TODO
    }
    message AnyCoroEndInst {
      // TODO
    }
    message AnyCoroIdInst {
      // TODO
    }
    message AnyCoroSuspendInst {
      // TODO
    }
    message AssumeInst {
      // TODO
    }
    message BinaryOpIntrinsic {
      message SaturatingInst {
        enum Operation {
          U_ADD = 0;
          S_ADD = 1;
          U_SUB = 2;
          S_SUB = 3;
        }
        Value result = 1;
        Operation operation = 2;
        Value l = 3;
        Value r = 4;
      }
      message WithOverflowInst {
        // TODO
      }

      SaturatingInst saturatingInst = 1;
      WithOverflowInst withOverflowInst = 2;
    }
    message ConstrainedFPIntrinsic {
      // TODO
    }
    message ConvergenceControlInst {
      // TODO
    }
    message CoroAlignInst {
      // TODO
    }
    message CoroAllocInst {
      // TODO
    }
    message CoroAllocaAllocInst {
      // TODO
    }
    message CoroAllocaFreeInst {
      // TODO
    }
    message CoroAllocaGetInst {
      // TODO
    }
    message CoroAsyncContextAllocInst {
      // TODO
    }
    message CoroAsyncContextDeallocInst {
      // TODO
    }
    message CoroAsyncResumeInst {
      // TODO
    }
    message CoroAsyncSizeReplace {
      // TODO
    }
    message CoroBeginInst {
      // TODO
    }
    message CoroEndResults {
      // TODO
    }
    message CoroFrameInst {
      // TODO
    }
    message CoroFreeInst {
      // TODO
    }
    message CoroPromiseInst {
      // TODO
    }
    message CoroSaveInst {
      // TODO
    }
    message CoroSizeInst {
      // TODO
    }
    message CoroSubFnInst {
      // TODO
    }
    message DbgInfoIntrinsic {
      // TODO
    }
    message GCProjectionInst {
      // TODO
    }
    message InstrProfInstBase {
      // TODO
    }
    message LifetimeIntrinsic {
      // TODO
    }
    message MinMaxIntrinsic {
      // TODO
    }
    message NoAliasScopeDeclInst {
      // TODO
    }
    oneof subclass {
      MemIntrinsicBase memIntrinsicBase = 1;
      AnyCoroEndInst anyCoroEndInst = 2;
      AnyCoroIdInst anyCoroIdInst = 3;
      AnyCoroSuspendInst anyCoroSuspendInst = 4;
      AssumeInst assumeInst = 5;
      BinaryOpIntrinsic binaryOpIntrinsic = 6;
      ConstrainedFPIntrinsic constrainedFPIntrinsic = 7;
      ConvergenceControlInst convergenceControlInst = 8;
      CoroAlignInst coroAlignInst = 9;
      CoroAllocInst coroAllocInst = 10;
      CoroAllocaAllocInst coroAllocaAllocInst = 11;
      CoroAllocaFreeInst coroAllocaFreeInst = 12;
      CoroAllocaGetInst coroAllocaGetInst = 13;
      CoroAsyncContextAllocInst coroAsyncContextAllocInst = 14;
      CoroAsyncContextDeallocInst coroAsyncContextDeallocInst = 15;
      CoroAsyncResumeInst coroAsyncResumeInst = 16;
      CoroAsyncSizeReplace coroAsyncSizeReplace = 17;
      CoroBeginInst coroBeginInst = 18;
      CoroEndResults coroEndResults = 19;
      CoroFrameInst coroFrameInst = 20;
      CoroFreeInst coroFreeInst = 21;
      CoroPromiseInst coroPromiseInst = 22;
      CoroSaveInst coroSaveInst = 23;
      CoroSizeInst coroSizeInst = 24;
      CoroSubFnInst coroSubFnInst = 25;
      DbgInfoIntrinsic dbgInfoIntrinsic = 26;
      GCProjectionInst gCProjectionInst = 27;
      InstrProfInstBase instrProfInstBase = 28;
      LifetimeIntrinsic lifetimeIntrinsic = 29;
      MinMaxIntrinsic minMaxIntrinsic = 30;
      NoAliasScopeDeclInst noAliasScopeDeclInst = 31;
    }
  }

  IntrinsicInst intrinsicInst = 1;
  CallInstClassic callInstClassic = 2;
}

message CGStatepointInst {
  // TODO without subclasses
}

message InvokeInst {
  Function function = 1;
  // TODO FunctionType can be here
  repeated Value arguments = 2;
  optional Value result = 3;

  string normalBranch = 4;
  string unwindBranch = 5;
}

message Operator {
  // TODO with subclasses
}

message UnaryInstruction {
  message ExtractValueInst {
    Value result = 1;
    Value target = 2;
    proto.type.Type targetType = 3;
    repeated Value operand = 4;
  }

  oneof subclass {
    AllocaInst allocaInst = 1;
    CastInst castInst = 2;
    // TODO with subclasses CastInst
    ExtractValueInst extractValueInst = 4;
    FreezeInst freezeInst = 5;
    LoadInst loadInst = 6;
    // TODO with subclasses UnaryOperator, VAArgInst
  }
}

message StoreInst {
  Value from = 1;
  Value to = 2;
}

message FreezeInst {

}

message LoadInst {
  Value from = 1;
  Value to = 2;
}

message AllocaInst {
  Value to = 1;
}

message CastInst {
  oneof subclass {
    AddrSpaceCastInst addrSpaceCastInst = 1;
    BitCastInst bitCastInst = 2;
    FPExtInst fPExtInst = 3;
    FPToSIInst fPToSIInst = 4;
    FPToUIInst fPToUlInst = 5;
    FPTruncInst fPTruncInst = 6;
    IntToPtrInst intToPtrInst = 7;
    PtrToIntInst ptrToIntInst = 9;
    SExtInst sExtInst = 10;
    SIToFPInst sIToFPInst = 11;
    TruncInst truncInst = 12;
    UIToFPInst uIToFPInst = 13;
    ZExtInst zExtInst = 14;
  }

  proto.type.Type fromType = 15;
  proto.type.Type toType = 16;
  Value from = 17;
  Value to = 18;

  message AddrSpaceCastInst {
    // TODO
  }
  message BitCastInst {
    // TODO
  }
  message FPExtInst {
    // TODO
  }
  message FPToSIInst {
    // TODO
  }
  message FPToUIInst {
    // TODO
  }
  message FPTruncInst {
    // TODO
  }
  message IntToPtrInst {
    // TODO
  }
  message PtrToIntInst {
    // TODO
  }
  message SExtInst {
  }
  message SIToFPInst {
    // TODO
  }
  message TruncInst {
  }
  message UIToFPInst {
  }
  message ZExtInst {
    // TODO
  }
}

message ReturnInst {
  optional Value return = 1;
}